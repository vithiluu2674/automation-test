package data_tests;

import org.testng.annotations.DataProvider;

public class DataCrowdsale {

	@DataProvider (name="data-buy-crowdsale")
	public Object[][] dataBuyCrowdsale(){
		double btcAmount=1;
		double ethAmount=1;
		double bnbAmount=1;
		double usdtAmount=1;
		double usdcAmount=1;
		return new Object[][] {{"BTC", btcAmount}, {"ETH", ethAmount},
			{"BNB", bnbAmount}, {"USDT", usdtAmount}, {"USDC", usdcAmount} };
	}
	
}
