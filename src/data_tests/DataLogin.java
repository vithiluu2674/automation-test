package data_tests;

import org.testng.annotations.DataProvider;

public class DataLogin {

	public String username="huetransky@gmail.com";
	public String password="Hue@12345";
	
	public DataLogin() {
		// TODO Auto-generated constructor stub
	}
	
	@DataProvider(name="login-success")
	public Object[][] dataLoginSuccess(){
		return new Object[][] {{"huetransky@gmail.com", "Hue@12345"}};
	}
	
	@DataProvider(name="login-invalid-email")
	public Object[][] dataLoginInvalidEmail(){
		return new Object[][] {{"hhh@hhh.com", "Hue@12345"}};
	}
	
	@DataProvider(name="login-invalid-password")
	public Object[][] dataLoginInvalidPassword(){
		return new Object[][] {{"huetransky@gmail.com", "12345"}};
	}
	
}
