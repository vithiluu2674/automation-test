package sign_tests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.Links;
import utilities.SetupDriver;


public class SignUpPage {
    public WebDriver driver;

    @FindBy (css="#app > div > div.form > form > div.flex_line.be-flex.respon-name.el-row > div:nth-child(1) > div.el-form-item > div > div.el-input > input")
    public WebElement firstNameTextBox;
    
    @FindBy (css="#app > div > div.form > form > div.flex_line.be-flex.respon-name.el-row > div:nth-child(2) > div.el-form-item > div > div.el-input > input")
    public WebElement lastNameTextBox;

    @FindBy (css="#app > div > div.form > form > div.el-form-item > div > div > input")
    public WebElement emailTextbox;

    @FindBy (css="#app > div > div.form > form > div.el-form-item.input-password > div > span:nth-child(1) > span > div > input")
    public WebElement passwordTextbox;

    @FindBy (css="#app > div > div.form > form > div:nth-child(8) > div > div > input")
    public WebElement referralCodeTextbox;

    @FindBy (css="#app > div > div.form > form > div > label.el-checkbox")
    public WebElement confirmCheckbox;

    @FindBy (css="#app > div > div.form > form > button")
    public WebElement submitButton;

    public SignUpPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}

    public void loadPage() {
		driver.get(Links.SIGN_UP_URL);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

    public void register(
        String firstName,
        String lastName,
        String email,
        String password,
        String referralCode 
    ) {
        firstNameTextBox.sendKeys(firstName);
        try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        lastNameTextBox.sendKeys(lastName);
        try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        emailTextbox.sendKeys(email);
        try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        passwordTextbox.sendKeys(password);
        try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        referralCodeTextbox.sendKeys(referralCode);
        try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		confirmCheckbox.click();
		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

        submitButton.click();
    }

    public static void main(String[] args) {
		WebDriver driver=SetupDriver.getDriver();
		SignUpPage page=new SignUpPage(driver);
		page.loadPage();
		page.register("Test", "hihi", "firstautotest@mailinator.com", "Vi@@1234", "");
	}
}
