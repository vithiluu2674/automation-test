package sign_tests;

import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import data_tests.DataLogin;
import utilities.CheckElement;
import utilities.Links;
import utilities.SetupDriver;

public class SignInTest {

	WebDriver driver;
	SignInPage signinPage;
	CheckElement checkElement;
	
	@BeforeMethod
	public void openPage() {
		signinPage=new SignInPage(SetupDriver.getDriver());
		checkElement=new CheckElement();
		this.driver=signinPage.driver;
		driver.manage().window().maximize();
		signinPage.loadPage();
	}
	
	@AfterMethod
	public void refreshPage() {
		driver.quit();
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
	
	// @AfterClass
	public void closePage() {
		signinPage.driver.quit();
	}
	
	@Test(enabled=true, dataProvider = "login-success", dataProviderClass = DataLogin.class)
	public void Test_LoginSuccess(String username, String password) {
		signinPage.login(username, password);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		Assert.assertTrue(driver.getCurrentUrl().contains(Links.CROWDSALE_URL));
	}
	
	@Test(enabled=false, dataProvider = "login-invalid-email", dataProviderClass = DataLogin.class)
	public void Test_LoginInvalidUsername(String username, String password) {
		signinPage.login(username, password);
		
	}
	@Test(enabled=false,dataProvider = "login-invalid-password", dataProviderClass = DataLogin.class)
	public void Test_LoginInvalidPassword(String username, String password) {
		signinPage.login(username, password);
		
	}

	@Test(enabled=true)
	public void Test_LoginBlank() {
		boolean checkBtn=checkElement.checkEnabled(signinPage.loginButton);
		Assert.assertEquals(checkBtn, true);
	}
	
}
