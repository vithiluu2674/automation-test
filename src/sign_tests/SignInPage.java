package sign_tests;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utilities.Links;
import utilities.SetupDriver;

public class SignInPage {

	public WebDriver driver;
	
	@FindBy (css="#app > div > div.form > form > div:nth-child(2) > div > div > input")
	public WebElement userNameTextBox;
	
	@FindBy (css="div.input-password.el-input > input")
	public WebElement passwordTextBox;
	
	@FindBy (css="#app > div > div.form > form > button")
	public WebElement loginButton;
	
	public SignInPage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
		
	}
	
	public void loadPage() {
		driver.manage().window().maximize();
		driver.get(Links.LOGIN_URL);
		try {
			Thread.sleep(5000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void login(String username, String password) {
		userNameTextBox.sendKeys(username);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		passwordTextBox.sendKeys(password);
		try {
			Thread.sleep(3000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		List<WebElement> frames = driver.findElements(By.tagName("iframe"));
//	    String winHanaleBefore = driver.getWindowHandle();
//	    driver.switchTo().frame(0);
//	   driver.findElement(By.id("recaptcha-anchor")).click();
//	   driver.switchTo().window(winHanaleBefore);
//		try {
//			Thread.sleep(3000);
//		} catch (InterruptedException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
		loginButton.click();
	}
	
	public static void main(String[] args) {
		WebDriver driver=SetupDriver.getDriver();
		SignInPage page=new SignInPage(driver);
		page.loadPage();
		page.login("huetransky@gmail.com", "Hue@12345");
	}
}
