package utilities;

import org.jtwig.JtwigModel;
import org.jtwig.JtwigTemplate;

import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;


public class ListAPIToTest {
	
	public static String getCode(String email, String phone) {
		String code="";
		String getCodeAPI=Links.DOMAIN_API+"/api/test?emailOrPhone=";
		if(phone==null) {
			getCodeAPI=getCodeAPI+email;
		}else {
			getCodeAPI=getCodeAPI+phone;
		}
		try {
			JsonNode body=Unirest.post(getCodeAPI)
			.header("accept", "application/json")
			.header("Content-Type", "application/json")
			.asJson().getBody();
			
			code=(String) body.getObject().get("data");
			System.out.println(body);
		} catch (UnirestException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return code;
	}
	
	public static void main(String[] args) {
		System.out.println(getCode("huetransky@gmail.com", null));

	}
}
