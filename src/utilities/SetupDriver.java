package utilities;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;
	
public class SetupDriver {
	
	public static void setWindow(WebDriver driver) {
		driver.manage().window().maximize();
	}
	
	public static WebDriver getDriver() {
		WebDriver driver = null;
		try {
			System.setProperty("webdriver.chrome.whitelistedIps", "");
			WebDriverManager.chromedriver().setup();
			driver=new ChromeDriver();
			driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		} catch (Exception e) {
			System.out.println("Can't open browser");
		}
		return driver;
	}
}
