package utilities;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.Color;

public class CheckElement {
	public static boolean checkDisplayed(WebElement element) {
		return element.isDisplayed();
	}
	
	public static boolean checkEnabled(WebElement element) {
		return element.isEnabled();
	}
	
	public static boolean checkSelected(WebElement element) {
		return element.isSelected();
	}
	
	public static boolean checkText(WebElement element, String text) {
		boolean check= element.getText().trim().equals(text);
		return check;
	}
	
	public static boolean checkSize(WebElement element, int width, int height ) {
		Dimension d=element.getSize();
		if(width == -1) {
			return d.height==height;
		}else if(height == -1) {
			return d.width==width;
		}
		return d.width==width && d.height==height;
	}
	
	public static boolean checkColor(WebElement element, String cssValue, String colorCode) {
		String color =  element.getCssValue(cssValue);
		String hex = Color.fromString(color).asHex();
		return hex.equals(colorCode);
	}
	
	public static boolean checkFont(WebElement element, String fontName) {
		return element.getCssValue("font-family").equals(fontName);
	}
	
	public static void main(String[] args) {
		
	}
	
}
