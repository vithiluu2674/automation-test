package utilities;

import java.io.File;

public class Links {
	public static final String DOMAIN_API="https://test-blockchain-api.beedu.vn";
	public static final String DOMAIN = "https://test-blockchain-web.beedu.vn";
	public static final String LOGIN_URL = DOMAIN + "/login";
	public static final String SIGN_UP_URL = DOMAIN + "/signup";
	public static final String CROWDSALE_URL= DOMAIN + "/crowd-sale/";
}
