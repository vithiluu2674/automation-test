package crowdsale_tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import data_tests.DataCrowdsale;
import data_tests.DataLogin;
import sign_tests.SignInPage;
import utilities.SetupDriver;

public class CrowdsaleTest {

	WebDriver driver;
	SignInPage signInPage;
	CrowdsalePage crowdsalePage;
	WebDriverWait wait;
	
	@BeforeClass
	public void openPage() {
		signInPage = new SignInPage(SetupDriver.getDriver());
		wait=new WebDriverWait(driver, 30);
		this.driver=signInPage.driver;
		crowdsalePage=new CrowdsalePage(this.driver);
		driver.manage().window().maximize();
		signInPage.loadPage();
		signInPage.login(new DataLogin().username, new DataLogin().password);
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	}
	
	// @AfterClass
	public void closePage() {
		driver.quit();
	}
	
	@Test(enabled = true, dataProvider = "data-buy-crowdsale", dataProviderClass = DataCrowdsale.class)
	public void TestBuyToken(String currency, double tokenAmount) {
		crowdsalePage.buy(currency, tokenAmount, 0);
		
	}
	
}

