package crowdsale_tests;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import sign_tests.SignInPage;
import utilities.SetupDriver;

public class CrowdsalePage {

	WebDriver driver;
	
//	@FindBy (css="")
//	public WebElement buyTitle;
	
	@FindBy (css="form.form-buy-lyn > div.list-wallet > div > div > div.el-input > input")
	public WebElement paidTokenSelectBox;
	
	@FindBy (css="form.form-buy-lyn > div:nth-child(2) > div > div.el-input > input")
	public WebElement paidAmountTextBox;
	
	@FindBy (css="form.form-buy-lyn > div:nth-child(4) > div > div.el-input > input")
	public WebElement tokenAmountTextBox;
	
	@FindBy (css="form.form-buy-lyn > button")
	public WebElement buyButton;
	
	public CrowdsalePage(WebDriver driver) {
		this.driver=driver;
		PageFactory.initElements(driver, this);
	}
	
	public void buy(String currency, double paidAmount, double tokenAmount) {
		WebDriverWait wait=new WebDriverWait(driver, 20);
		driver.manage().timeouts().implicitlyWait(1, TimeUnit.MINUTES);
		
		WebElement icon=driver.findElement(By.cssSelector("form.form-buy-lyn > div.list-wallet > div > div > div.el-input.el-input--prefix.el-input--suffix > span.el-input__suffix > span > i"));
		icon.click();
		
		/*
		if(currency != "BTC") {
			
			WebElement icon=driver.findElement(By.cssSelector("form.form-buy-lyn > div.list-wallet > div > div > div.el-input.el-input--prefix.el-input--suffix > span.el-input__suffix > span > i"));
			
			
			WebElement selectList= driver.findElement(By.cssSelector("ul.el-select-dropdown__list"));
			if(currency == "ETH") {
				WebElement eth= selectList.findElement(By.cssSelector("li:nth-child(2) > div"));
				eth.click();
			} else if (currency == "USDT") {
				WebElement usdt= selectList.findElement(By.cssSelector("li:nth-child(3) > div"));
				usdt.click();
			} else if (currency == "USDC") {
				WebElement usdc= selectList.findElement(By.cssSelector("li:nth-child(4) > div"));
				usdc.click();
			} else if (currency == "BNB") {
				WebElement bnb= selectList.findElement(By.cssSelector("li:nth-child(5) > div"));
				bnb.click();
			}
		}
		*/
//		paidAmountTextBox.sendKeys(paidAmount);
//		
//		buyButton.click();
	}
	public static void main(String[] args) {
		SignInPage signInPage=new SignInPage(SetupDriver.getDriver());
		signInPage.loadPage();
		signInPage.login("huetransky@gmail.com", "Hue@12345");
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		CrowdsalePage page=new CrowdsalePage(signInPage.driver);
		page.buy("ETH", 0, 0);
		
	}
}
